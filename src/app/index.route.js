export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'vm'
    })
    .state('success', {
      url: '/success',
      params: {
        message: 'Операция выполнена успешно'
      },
      template: '<success message="vm.message"></success>',
      controller($log, $stateParams) {
        this.message = $stateParams.message;
      },
      controllerAs: 'vm'
    });

  $urlRouterProvider.otherwise('/');
}
