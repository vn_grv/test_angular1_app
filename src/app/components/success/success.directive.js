export function Success() {
  'ngInject';

  const directive = {
    restrict: 'E',
    templateUrl: 'app/components/success/success.html',
    scope: {
      message: '<'
    },
    controller: SuccessController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class SuccessController {
  constructor($element) {
    'ngInject';

    this.$element = $element;
  }

  $postLink() {
    this.$element.addClass('success');
  }
}
