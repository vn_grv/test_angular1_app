export function SdInput() {
  'ngInject';

  const directive = {
    restrict: 'E',
    require: '?ngModel',
    scope: {
      name: '@',
      type: '@',
      label: '@',
      placeholder: '@'
    },
    templateUrl: 'app/components/sd-input/sd-input.html',
    link: linkFunc
  };

  return directive;

  function linkFunc(scope, el, attrs, ngModel) {
    el.addClass('sd-input');
    el.find('input').attr('type', scope.type);
    if (scope.placeholder) {
      el.find('input').attr('placeholder', scope.placeholder);
    }
    el.find('label').text(scope.label);

    el.on('click', () => {
      ngModel.$touched = true;
    });

    if (!ngModel) return;

    if (attrs.required) {
      ngModel.$validators.required = (modelValue, viewValue) => {
        const value = modelValue || viewValue;
        return value ? true : false;
      };
    }

    if (scope.type === 'email') {
      // http://habrahabr.ru/post/175375/
      const EMAIL_REGEXP = /.+@.+\..+/i;
      ngModel.$validators.email = (modelValue, viewValue) => {
        const value = modelValue || viewValue;
        return EMAIL_REGEXP.test(value);
      };
    }

    ngModel.$render = () => {
      scope.value = ngModel.$viewValue;
    };

    scope.$watch('value', value => {
      ngModel.$setViewValue(value);
    });
  }
}
