export function LoginDirective() {
  'ngInject';

  const directive = {
    restrict: 'E',
    templateUrl: 'app/components/auth/login/login.html',
    scope: {},
    controller: LoginController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class LoginController {
  constructor($log, $element, authService, $state) {
    'ngInject';

    this.$log = $log;
    this.$element = $element;
    this.$state = $state;
    this.user = {};
    this.authService = authService;
    this.sendingAuthForm = false;
  }

  $postLink() {
    this.$element.addClass('login');
  }

  login(user) {
    this.sendingAuthForm = true;
    this.authService
      .login(user)
      .then(() => {
        this.$state.go('success', { message: 'Ваше сообщение отправлено' });
      })
      .catch(reason => {
        this.$log.error(reason);
      })
      .finally(() => {
        this.sendingAuthForm = false;
      });
  }
}
