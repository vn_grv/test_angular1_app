export class AuthService {
  constructor($log, $q, $timeout) {
    'ngInject';

    this.$log = $log;
    this.$q = $q;
    this.$timeout = $timeout;
  }

  login(user) {
    this.$log.log('login', user);
    const deferred = this.$q.defer();
    const resp = {};
    this.$timeout(() => {
      deferred.resolve(resp);
    }, 2000);

    // const resp = { message: 'Ошибка валидации email' };
    // deferred.reject(resp);
    return deferred.promise;
  }
}
