export function AuthFormDirective() {
  'ngInject';

  const directive = {
    restrict: 'E',
    templateUrl: 'app/components/auth/auth-form/auth-form.html',
    scope: {
      user: '<',
      button: '@',
      onSubmit: '&',
      processing: '<'
    },
    controller: AuthFormController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class AuthFormController {
  constructor($log, $element) {
    'ngInject';
    this.$log = $log;
    this.$element = $element;
    this.authForm;
    this.showAuthErrors = false;
  }

  $postLink() {
    this.$element.addClass('auth-form');
  }

  canShowFieldErrorClass(fieldName) {
    if (!this.authForm[fieldName]) {
      return;
    }
    if (this.authForm[fieldName].$touched) {
      return this.authForm[fieldName].$invalid;
    }
    if (this.authForm.$submitted && this.showAuthErrors) {
      return this.authForm[fieldName].$invalid;
    }
    return false;
  }

  submit() {
    this.showAuthErrors = false;
    if (!this.authForm.$valid) {
      this.showAuthErrors = true;
    }
    this.onSubmit({ user: this.user });
  }
}
