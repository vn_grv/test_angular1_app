import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { LoginDirective } from '../app/components/auth/login/login.directive';
import { AuthFormDirective } from '../app/components/auth/auth-form/auth-form.directive';
import { SdInput } from '../app/components/sd-input/sd-input.directive';
import { AuthService } from '../app/components/auth/auth.service';
import { Success } from '../app/components/success/success.directive';

angular
  .module('gulpAngularExample', [
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ui.router'
  ])
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('MainController', MainController)
  .directive('login', LoginDirective)
  .directive('authForm', AuthFormDirective)
  .directive('sdInput', SdInput)
  .directive('success', Success)
  .service('authService', AuthService);
